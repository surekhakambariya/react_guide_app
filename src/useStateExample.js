const App = props => {

    const [ personsState, setPersonsState ] = useState({
      persons: [
        { name: 'Mayur', age: '26' },
        { name: 'Surekha', age: '23' },
        { name: 'Manu', age: '29' }
      ],
      otherState: 'Other states'
    });
  
    const switchNameHandler = () => {
      // console.log('Was clicked!')
      setPersonsState({
        persons: [
          { name: 'Mayur Kambariya', age: '26' },
          { name: 'Surekha Kambariya', age: '23' },
          { name: 'Manu', age: '29' }
        ]
      });
    };
  
    return (
      <div className="App">
        <h1>I'm React App</h1>
        <p>This is really working!</p>
        <button onClick={switchNameHandler}>Switch Name</button>
        <Person
          name={personsState.persons[0].name}
          age={personsState.persons[0].age}
        />
        <Person
          name={personsState.persons[1].name}
          age={personsState.persons[1].age}
          click={switchNameHandler} >My Hobbies: Racing </Person>
        <Person
          name={personsState.persons[2].name}
          age={personsState.persons[2].age}
        />
      </div>
    );
    // return React.createElement('div', {className: 'App'}, createElement('h1', null, 'I\'m React App'))
  }